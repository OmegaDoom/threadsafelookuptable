#ifndef THREAD_SAFE_LOOKUP_TABLE_HPP
#define THREAD_SAFE_LOOKUP_TABLE_HPP

#include <vector>
#include <list>
#include <algorithm>
#include <functional>
//#include <boost/thread/shared_mutex.hpp>
#include <shared_mutex>

template<typename TKey, typename TValue, typename THash = std::hash<TKey>>
class ThreadSafeLookupTable
{
	typedef std::pair<TKey, TValue> bucket_value_type;
public:
	ThreadSafeLookupTable(const unsigned buckets = 19, const THash& hasher = THash())
		: m_buckets(buckets)
		, m_hasher(hasher)
	{
	}

	TValue get(const TKey& key, const TValue& default_value = TValue()) const
	{
		return get_bucket(key).value_for(key, default_value);
	} 
	
	void add_or_update(const TKey& key, const TValue& value)
	{
		get_bucket(key).add_or_update(key, value);
	}

	void remove(const TKey& key)
	{
		get_bucket(key).remove(key);
	}

private:
	typedef boost::shared_mutex lock_type;
	
	class bucket_entry
	{
		typedef typename std::list<bucket_value_type>::iterator bucket_iterator;
		typedef typename std::list<bucket_value_type>::const_iterator bucket_const_iterator;

		bucket_const_iterator find_value(const TKey& key) const
		{
			return std::find_if(m_list.cbegin(), m_list.cend()
				, [&key] (const bucket_value_type& value) { return value.first == key; }); 
		}

		bucket_iterator find_value(const TKey& key)
		{
			return std::find_if(m_list.begin(), m_list.end()
				, [&key] (const bucket_value_type& value) { return value.first == key; }); 
		}

	public:
		TValue value_for(const TKey& key, const TValue& default_value) const
		{
			std::shared_lock<lock_type> guard(m_mutex);
			auto iter = find_value(key);
			return iter != m_list.cend() ? iter->second : default_value;
		}

		void add_or_update(const TKey& key, const TValue& value)
		{
			std::lock_guard<lock_type> guard(m_mutex);
			auto iter = find_value(key);
			if (iter != m_list.end())
			{
				iter->second = value; 
			}
			else
			{
				m_list.emplace_back(key, value);
			}
		}

		void remove(const TKey& key)
		{
			std::lock_guard<lock_type> guard(m_mutex);
			m_list.remove_if([&key] (const bucket_value_type& value) { return value.first == key; }); 
		}
		
	private:
		std::list<bucket_value_type> m_list;
		mutable lock_type m_mutex;
	};
	
	bucket_entry& get_bucket(const TKey& key)
	{
		return m_buckets[m_hasher(key) % m_buckets.size()]; 
	}

	const bucket_entry& get_bucket(const TKey& key) const
	{
		return m_buckets[m_hasher(key) % m_buckets.size()]; 
	}

	std::vector<bucket_entry> m_buckets;
	THash m_hasher;
};

#endif //THREAD_SAFE_LOOKUP_TABLE_HPP
