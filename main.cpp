#include <iostream>
#include <string>
#include <thread>
#include <atomic>
#include "ThreadSafeLookupTable.hpp"

#define ITERATIONS 100000
#define ITEMS 1000

typedef unsigned long long sum_type;

ThreadSafeLookupTable<int, std::string> lookup(19);
std::atomic<bool> start_reading(false);

void writer(sum_type& sum)
{
	bool adding = false;
	start_reading.store(true, std::memory_order_relaxed);
	for (int i = 0; i < ITERATIONS; ++i)
	{
		int index = i % ITEMS;
		if (0 == index)
		{
			adding = !adding;			
		}

		float value = 0.555 * i;

		if (adding)
			lookup.add_or_update(index, std::to_string(value));
		else
			lookup.remove(index);

		sum += value;
	}
}

void reader(sum_type& sum)
{
	while(!start_reading.load(std::memory_order_relaxed));

	for (int i = 0; i < 4 * ITERATIONS; ++i)
	{
		int index = i % ITEMS;
		auto str = lookup.get(index);
		if (!str.empty())
		{
			auto value = std::stof(str);	
			sum += value;
		}
	}
}

int main(int argc, char* argv[])
{
	sum_type sum1 = 0;
	sum_type sum2 = 0;
	sum_type sum3 = 0;

	std::thread thr1(reader, std::ref(sum1));
	std::thread thr2(writer, std::ref(sum2));
	std::thread thr3(writer, std::ref(sum3));

	thr1.join();
	thr2.join();
	thr3.join();
	
	bool equal = sum1 == sum2 + sum3;
	std::cout << "write sum = " << sum2 + sum3 << " read sum " << sum1 << " equal = " << std::boolalpha << equal;
}
