# -Makefile-

CXX := g++
CXXFLAGS := -std=c++17 -g

main: main.o
	$(CXX) $? -o $@

main.o: main.cpp ThreadSafeLookupTable.hpp
	$(CXX) $(CXXFLAGS) -c $^

.PHONY:
clean:
	rm *.o main
